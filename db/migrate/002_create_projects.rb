class CreateProjects < ActiveRecord::Migration
  def self.up
    create_table :projects do |t|
      t.string :name
      t.integer :category_id
      t.integer :client_id
      t.datetime :release_date
      t.string :url
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :projects
  end
end
